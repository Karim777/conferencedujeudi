/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database.POJO;

import static Database.DatabaseUtilities.getConnexion;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author karim
 */
public class Conference {

    private int idConference;
    private String titreConference;
    private Calendar dateConference;
    private int idConferencier;
    private int idSalle;
    private int idTheme;
    private String nomConferencier;
    private Date sqlDate;

    public Conference() {

    }

    public Conference(int _idConference, String _titreConference, Calendar _dateConference, int _idConferencier) {
        this.idConference = _idConference;
        this.titreConference = _titreConference;
        this.dateConference = _dateConference;
        this.idConferencier = _idConferencier;
    }

    public Conference(int _idConference, String _titreConference, Calendar _dateConference, int _idConferencier, int _idSalle, int _idTheme, String _nomConferencier) {
        this.idConference = _idConference;
        this.titreConference = _titreConference;
        this.dateConference = _dateConference;
        this.idConferencier = _idConferencier;
        this.idSalle = _idSalle;
        this.idTheme = _idTheme;
        this.nomConferencier = _nomConferencier;
    }
    
    public Conference( String _titreConference, Calendar _dateConference, int _idConferencier, int _idSalle, int _idTheme) {
        
        this.titreConference = _titreConference;
        this.dateConference = _dateConference;
        this.idConferencier = _idConferencier;
        this.idSalle = _idSalle;
        this.idTheme = _idTheme;
       
    }
    
    public Conference(int _idConference, String _titreConference, Calendar _dateConference, int _idConferencier, int _idSalle, int _idTheme) {
        this.idConference=_idConference;
        this.titreConference = _titreConference;
        this.dateConference = _dateConference;
        this.idConferencier = _idConferencier;
        this.idSalle = _idSalle;
        this.idTheme = _idTheme;
       
    }

    public int getidConference() {
        return this.idConference;
    }

    public void setidConference(int _idConference) {
        this.idConference = _idConference;
    }

    public String getTitreConference() {
        return this.titreConference;
    }

    public void setTitreConference(String _titreConference) {
        this.titreConference = _titreConference;
    }

    public Calendar getDateCalendar() {
        return this.dateConference;
    }

    public void setDateCalendar(Calendar _dateConference) {
        this.dateConference = _dateConference;
    }

    public int getidConferencier() {
        return this.idConferencier;
    }

    public void setidConferencier(int _idConferencier) {
        this.idConferencier = _idConferencier;
    }

    public int getidSalle() {
        return this.idSalle;
    }

    public void setidSalle(int _idSalle) {
        this.idSalle = _idSalle;
    }

    public int getidTheme() {
        return this.idTheme;
    }

    public void setidTheme(int _idTheme) {
        this.idTheme = _idTheme;
    }

    public void setnomConferencier(String _nomConferencier) {
        this.nomConferencier = _nomConferencier;
    }

    public String getnomConferencier() {
        return this.nomConferencier;
    }

    public String getDateString() {
        Calendar cal = this.dateConference;
        SimpleDateFormat format1 = new SimpleDateFormat("dd-MM-yyy");
        String formatted = format1.format(cal.getTime());
        return formatted;
    }
    
     public java.sql.Date getSqlDate() {
            java.sql.Date dateSql= new java.sql.Date(dateConference.getTimeInMillis());
        return dateSql;
    }

   
    
    public void insert(){
        PreparedStatement ps=null;
        Connection maconnexion=getConnexion();
        String query="INSERT INTO conferences"+"(Titre,Date,idConferencier,idSalle,idTheme)"+ "VALUES(?,?,?,?,?)";
        
try{
    ps= maconnexion.prepareStatement(query);
    ps.setString(1, this.getTitreConference());
    ps.setDate(2, this.getSqlDate());
    ps.setInt(3, this.getidConferencier());
    ps.setInt(4, this.getidTheme());
    ps.setInt(5, this.getidSalle());
    
    ps.execute();
    
}  catch (SQLException ex){
    System.err.println(ex.getMessage());
}  
    }
}
