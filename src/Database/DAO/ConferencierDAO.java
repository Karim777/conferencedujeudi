/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database.DAO;

import static Database.DatabaseUtilities.exec;
import static Database.DatabaseUtilities.getConnexion;
import Database.POJO.Conference;
import Database.POJO.Conferencier;
import conferencedujeudi.ConferenceDuJeudi;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author karim
 */
public class ConferencierDAO {
     public static void update(Conferencier confe){
         Calendar ad=Calendar.getInstance();
     ResultSet resultat=null;
         
     
      
        PreparedStatement ps=null;
        Connection maconnexion=getConnexion();
        String query="UPDATE  conferences SET Titre=?,Date=?,idConferencier=?,idSalle=?,idTheme=? WHERE id=?";
        
try{
    ps= maconnexion.prepareStatement(query);
    ps.setString(1, conf.getTitreConference());
    ps.setDate(2, conf.getSqlDate());
    ps.setInt(3, conf.getidConferencier());
    ps.setInt(4, conf.getidTheme());
    ps.setInt(5, conf.getidSalle());
    ps.setInt(6, conf.getidConference());
    if(ps.execute()){
   resultat=ps.executeQuery();
    }
}  catch (SQLException ex){
    System.err.println(ex.getMessage());
}  
}
    
    public static void delete(int id) {
     
        ResultSet resultat=null;
         PreparedStatement ps=null;
        
        Connection maconnexion=getConnexion();
        String query="DELETE FROM conferences WHERE id=?";
        
try{
    ps= maconnexion.prepareStatement(query);
    ps.setInt(1,id);
   
 
    if (ps.execute()){
        resultat=ps.executeQuery();
    }
   
    
}  catch (SQLException ex){
    System.err.println(ex.getMessage());
}  
    
        
    }

    
     public static void insert(Conference conf){
         Calendar ad=Calendar.getInstance();
     ResultSet resultat=null;
         conf = new Conference("conference755",ad,1,1,1);
     
      
        PreparedStatement ps=null;
        Connection maconnexion=getConnexion();
        String query="INSERT INTO conferences"+"(Titre,Date,idConferencier,idSalle,idTheme)"+ "VALUES(?,?,?,?,?)";
        
try{
    ps= maconnexion.prepareStatement(query);
    ps.setString(1, conf.getTitreConference());
    ps.setDate(2, conf.getSqlDate());
    ps.setInt(3, conf.getidConferencier());
    ps.setInt(4, conf.getidTheme());
    ps.setInt(5, conf.getidSalle());
    if(ps.execute()){
   resultat=ps.executeQuery();
    }
}  catch (SQLException ex){
    System.err.println(ex.getMessage());
}  
}

    public static ArrayList<Conference> getAll() {
        ArrayList<Conference> listeConference = new ArrayList<>();
        ResultSet resultat = exec("select * from conferences");
        try {

            while (resultat.next()) {
                int id = resultat.getInt("id");
                String titre = resultat.getString("Titre");
                java.sql.Date date = resultat.getDate("Date");
                int idSalle = resultat.getInt("idSalle");
                int idTheme = resultat.getInt("idTheme");
                int idConferencier = resultat.getInt("idConferencier");
                int row = resultat.getRow();
                Calendar ad = Calendar.getInstance();
                ad.setTimeInMillis(date.getTime());
                //  System.out.println( id+titre+ date);
                Conference ca = new Conference(id, titre, ad, idSalle, idTheme, idConferencier);
                listeConference.add(ca);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ConferenceDuJeudi.class.getName()).log(Level.SEVERE, null, ex);
        }
        return (ArrayList<Conference>) listeConference;

    }

    public static Conference get(int id) {
        Conference c1=null;
        ResultSet resultat=null;
         PreparedStatement ps=null;
        
        Connection maconnexion=getConnexion();
        String query="SELECT * from conferences where id=?";
        
try{
    ps= maconnexion.prepareStatement(query);
    ps.setInt(1,id);
    resultat=ps.executeQuery();
 
    
   
    
}  catch (SQLException ex){
    System.err.println(ex.getMessage());
}  
    
         try {

            while (resultat.next()) {
                int idConf = resultat.getInt("id");
                String titre = resultat.getString("Titre");
                java.sql.Date date = resultat.getDate("Date");
                int idSalle = resultat.getInt("idSalle");
                int idTheme = resultat.getInt("idTheme");
                int idConferencier = resultat.getInt("idConferencier");
                Calendar ad = Calendar.getInstance();
                ad.setTimeInMillis(date.getTime());
                c1 = new Conference(idConf, titre, ad, idSalle, idTheme, idConferencier);

            }
        } catch (SQLException ex) {
            Logger.getLogger(ConferenceDuJeudi.class.getName()).log(Level.SEVERE, null, ex);
        }
        return c1;

    }
   
}
