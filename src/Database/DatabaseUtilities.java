/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database;

import static Database.DatabaseUtilities.getConnexion;
import com.mysql.jdbc.Connection;
import static java.lang.Class.forName;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author karim
 */
public class DatabaseUtilities {
    public static Connection getConnexion(){
         
        try{
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e){
            e.printStackTrace();
            return null;
        }
            Connection connection=null;
            try{
                connection=(Connection) DriverManager
                        .getConnection("jdbc:mysql://localhost:3306/conference","root","");
            }catch (SQLException e){
                e.printStackTrace();
                return null;
            }
        
   
    return connection;
    
}
    public static ResultSet exec(String query){
      Connection maconnection = getConnexion();
      
        Statement statement=null;
        try {
            statement=maconnection.createStatement();
            return statement.executeQuery(query);
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseUtilities.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
        
    }
}