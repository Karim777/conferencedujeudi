/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import Database.POJO.Conference;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author karim
 */
public class ConferenceModel extends AbstractTableModel {
    private final String[] columnName = {"Titre","Date","Conferencier"};

    private ArrayList<Conference> listeConference= new ArrayList<>();
    
   
    public ConferenceModel(ArrayList<Conference> _listeConference){
    this.listeConference= _listeConference;
}
    @Override
    public int getRowCount() {
       return listeConference.size();
    }

    @Override
    public int getColumnCount() {
        return 3;
    }
    
    @Override
    public String getColumnName(int columnIndex){
        return columnName[columnIndex];
        
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Conference maConference= listeConference.get(rowIndex);
        switch (columnIndex) {

		case 0:
			// Nom
			return maConference.getTitreConference();

		case 1:
			// Prenom
			return maConference.getDateString();

		case 2:
			// Annee
			return maConference.getnomConferencier();

		

		default:
			throw new IllegalArgumentException();
		}
    }
    
    
    
}
