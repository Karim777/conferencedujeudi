/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Panels;

import Database.DAO.ConferenceDAO;
import Database.DAO.ConferenceStatutDAO;
import Database.DAO.ConferencierDAO;
import Database.DatabaseUtilities;
import static Database.DatabaseUtilities.exec;
import Models.ConferenceModel;
import Database.POJO.Conference;
import Database.POJO.ConferenceStatut;
import Database.POJO.Conferencier;
import conferencedujeudi.ConferenceDuJeudi;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

/**
 *
 * @author karim
 */
public class ListeConference extends JPanel {

    Box pan1 = Box.createVerticalBox();

    Box pan2 = Box.createHorizontalBox();
    Box pan4 = Box.createHorizontalBox();

    public ListeConference() {
        JLabel label = new JLabel("Liste des Conférences");
        pan2.add(label);
        pan1.add(pan2);
        /**
         * Calendar cal = Calendar.getInstance(); cal.set(2019, 05, 23);
         * Conference c1 = new Conference(1, "Karim", cal, 1, 1, 1, "karim");
         * Calendar cal2 = Calendar.getInstance(); cal.set(2019, 05, 23);
         * Conference c2 = new Conference(1, "stanisal", cal2, 1, 1, 1, "jon");*
         */
        ArrayList<Conference> listeConference = ConferenceDAO.getAll();
//        Conference co=ConferenceDAO.get(1);
//        System.out.println(co.getTitreConference());
//      ConferenceDAO.insert(co);
//        ConferenceDAO.delete(6);
//        Calendar cale=Calendar.getInstance();
//        Conference updateConf = new Conference("ok", cale,6,6,6,1);
//        ConferenceDAO.update(updateConf);
//        Calendar cales = Calendar.getInstance();
//
//        ConferenceStatut updateStatutConf = ConferenceStatutDAO.get(1, 1);
//
//        cales.set(2019, 04, 23);
//        updateStatutConf.setDateStatutConference(cales);
//        ConferenceStatutDAO.update(updateStatutConf);

         Calendar ad=Calendar.getInstance();
         Conferencier co = ConferencierDAO.get(3);
         Conference conf = new Conference("conference666",ad,co,1,1);
         ConferenceDAO.insert(conf);
         
//        try{
//            
//            String query="select * from conferences";
//            ResultSet resultat= exec(query);
//            
//            while(resultat.next()){
//                int id=resultat.getInt("id");
//                String titre=resultat.getString("Titre");s
//                java.sql.Date date=resultat.getDate("Date");
//                int idSalle=resultat.getInt("idSalle");
//                int idTheme=resultat.getInt("idTheme");
//                int idConferencier=resultat.getInt("idConferencier");
//                int row=resultat.getRow();
//                Calendar ad=Calendar.getInstance();
//                ad.setTimeInMillis(date.getTime());
//              //  System.out.println( id+titre+ date);
//              Conference ca =new Conference(id,titre,ad,idSalle,idTheme,idConferencier);
//               listeConference.add(ca);
//            }} catch (SQLException ex) {
//            Logger.getLogger(ConferenceDuJeudi.class.getName()).log(Level.SEVERE, null, ex);
//        }
        // listeConference.add(c1);
        // listeConference.add(c2);
        ConferenceModel conferenceModel = new ConferenceModel(listeConference);

        JTable table = new JTable(conferenceModel);
        table.setSize(320, 400);
        table.setAutoCreateRowSorter(true);
        JScrollPane scrollpane = new JScrollPane(table);
        pan4.add(scrollpane);
        pan1.add(pan4);
        this.add(pan1);
        pan1.add(Box.createRigidArea(new Dimension(1400, 100)));

    }

}
