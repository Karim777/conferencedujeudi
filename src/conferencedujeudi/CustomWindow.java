/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conferencedujeudi;

import Panels.Home;
import Panels.ListeConference;
import java.awt.Color;
import java.awt.PopupMenu;
import static java.awt.SystemColor.menu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

/**
 *
 * @author karim
 */
public class CustomWindow extends JFrame implements ActionListener {

    private JMenuItem close;
     private JMenuItem consulter;
    private boolean bouton;
private JMenuItem accueil;
    public CustomWindow() {

        JMenu fichier, conférences;
        JMenuItem i2, i3;
        this.setTitle("Application AD");
        
        this.setSize(1000,1200);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JMenuBar menuBar = new JMenuBar();
         this.setVisible(true);
        this.setJMenuBar(menuBar);
        fichier = new JMenu("Fichier");
        JMenu home = new JMenu("Home");
        menuBar.add(home);
        accueil= new JMenuItem("accueil");
        conférences = new JMenu("Conférence");
        menuBar.add(fichier);
        menuBar.add(conférences);
        close = new JMenuItem("Fermer");
        consulter = new JMenuItem("Voir la liste des conférences");
        i3 = new JMenuItem("Ajouter une conférence");
        fichier.add(close);
        home.add(accueil);
        conférences.add(consulter);
        consulter.addActionListener(this);
        accueil.addActionListener(this);
        conférences.add(i3);
        close.addActionListener(this);
       
        
       //* Creation d'une instance de home afin de l'afficher dans une fenetre
        Home panel = new Home();

        this.setContentPane(panel);
        this.repaint();
        this.revalidate();
        

        this.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == close) {
            System.exit(0);
           
        }
        if  (e.getSource() == consulter){
              ListeConference panel2 = new ListeConference();

        this.setContentPane(panel2);
        this.repaint();
        this.revalidate();
        
         }
        
         if  (e.getSource() == accueil){
            Home panel = new Home();

        this.setContentPane(panel);
        this.repaint();
        this.revalidate();
    }

}
}